package de.danber.mydirection;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Handler;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

class GraphManager {
    private static int pointRadius = 3;
    private static int graphSize = 400;
    private static LinkedList<float[]> locationPoints = new LinkedList<>();
    private static LinkedList<float[]> heightPoints = new LinkedList<>();
    private static LinkedList<float[]> speedPoints = new LinkedList<>();
    private static ActivityMain macy;
    private static Handler handler = new Handler();
    private static int accuracy_threshold_stats;
    private static int accuracy_threshold_height;
    private static int height_smoothing_window;
    private static int speed_smoothing_window;
    private static boolean readyToDraw = true;

    private static float calculatedHeightMin = Float.POSITIVE_INFINITY;
    private static float calculatedHeightMax = Float.POSITIVE_INFINITY;
    private static float calculatedSpeedMax = Float.POSITIVE_INFINITY;

    static void initialize(ActivityMain activityMain) {
        macy = activityMain;

        // Load thresholds
        SharedPreferences pref = macy.getSharedPreferences("SETTINGS", 0);
        accuracy_threshold_stats = pref.getInt("STATS_ACCURACY", 20);
        accuracy_threshold_height = pref.getInt("HEIGHT_ACCURACY", 15);
        height_smoothing_window = pref.getInt("HEIGHT_SMOOTHING", 5);
        speed_smoothing_window = pref.getInt("HEIGHT_SMOOTHING", 3);
    }

    static void onLocationChanged() {
        boolean pointAdded = false;

        if (CurrentLocation.getAccuracy() < accuracy_threshold_stats) {
            float x = (float) Math.toRadians(CurrentLocation.getLongitude());
            float y = (float) Math.toRadians(CurrentLocation.getLatitude());
            float[] lp = {x, y};
            locationPoints.add(lp);

            float[] sp = {StatsManager.getTravelled(), (float) CurrentLocation.getSpeed()};
            speedPoints.add(sp);

            pointAdded = true;
        }

        if (CurrentLocation.getAltitude() != null && CurrentLocation.getAccuracy() < accuracy_threshold_height) {
            float[] hp = {StatsManager.getTravelled(), (float) CurrentLocation.getAltitude().doubleValue()};
            heightPoints.add(hp);

            pointAdded = true;
        }

        if (pointAdded) {
            updateGraphsAndTextsThreaded();
        }
    }

    private static void updateWaypointsText() {
        TextView tvWp = macy.findViewById(R.id.text_daten_np1);
        if (tvWp != null) {
            String str = locationPoints.size() + "";
            tvWp.setText(str);
        }
    }

    private static void updateMinMaxHeightText() {
        TextView tvMax = macy.findViewById(R.id.text_daten_xh1);
        if (tvMax != null) {
            if (!Float.isInfinite(calculatedHeightMax)) {
                BigDecimal bd = new BigDecimal(calculatedHeightMax).setScale(0, RoundingMode.HALF_UP);
                tvMax.setText(bd.toString());
            } else {
                tvMax.setText(macy.getResources().getString(R.string.no_data));
            }
        }
        TextView tvMin = macy.findViewById(R.id.text_daten_mh1);
        if (tvMin != null) {
            if (!Float.isInfinite(calculatedHeightMin)) {
                BigDecimal bd = new BigDecimal(calculatedHeightMin).setScale(0, RoundingMode.HALF_UP);
                tvMin.setText(bd.toString());
            } else {
                tvMin.setText(macy.getResources().getString(R.string.no_data));
            }
        }
    }

    private static void updateMaxSpeedText() {
        TextView tvMax = macy.findViewById(R.id.text_daten_ms1);
        if (tvMax != null) {
            if (!Float.isInfinite(calculatedSpeedMax)) {
                BigDecimal bd = new BigDecimal(calculatedSpeedMax).setScale(1, RoundingMode.HALF_UP);
                tvMax.setText(bd.toString());
            } else {
                tvMax.setText(macy.getResources().getString(R.string.no_data));
            }
        }
    }

    static void updateGraphsAndTextsThreaded() {

        // Don't start calculations twice
        if (!readyToDraw) {
            return;
        }

        readyToDraw = false;

        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {

                handler.post(new Runnable() {
                    public void run() {
                        // Update waypoints text before starting graph calculations
                        updateWaypointsText();

                    }
                });

                final Bitmap locationGraph = drawGraph(locationPoints, graphSize, graphSize, "location");
                final Bitmap heightGraph = drawGraph(heightPoints, graphSize, graphSize / 4 * 3, "height");
                final Bitmap speedGraph = drawGraph(speedPoints, graphSize, graphSize / 4 * 3, "speed");

                handler.post(new Runnable() {
                    public void run() {
                        drawAllGraphs(locationGraph, heightGraph, speedGraph);
                        updateMinMaxHeightText();
                        updateMaxSpeedText();
                    }
                });

                readyToDraw = true;
            }
        });
        t.start();
    }

    private static void drawAllGraphs(Bitmap locationGraph, Bitmap heightGraph, Bitmap speedGraph) {
        ImageView locationView = macy.findViewById(R.id.graph_w);
        if (locationView != null) {
            locationView.setImageBitmap(locationGraph);
        }

        ImageView heightView = macy.findViewById(R.id.graph_h);
        if (heightView != null) {
            heightView.setImageBitmap(heightGraph);
        }

        ImageView speedView = macy.findViewById(R.id.graph_s);
        if (speedView != null) {
            speedView.setImageBitmap(speedGraph);
        }
    }

    private static Bitmap drawGraph(LinkedList<float[]> points, int sizeX, int sizeY, String type) {

        Bitmap bmp = Bitmap.createBitmap(sizeX, sizeY, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bmp);
        Paint paintGreen = new Paint();
        paintGreen.setAntiAlias(true);
        paintGreen.setColor(macy.getResources().getColor(R.color.green));
        Paint paintWhite = new Paint();
        paintWhite.setAntiAlias(true);
        paintWhite.setColor(macy.getResources().getColor(R.color.white));


        if (points.size() == 1) {
            // No need for calculations if there is only one point
            canvas.drawCircle(sizeX / 2, sizeY / 2, pointRadius, paintGreen);

            if (type.equals("height")) {
                // Update min and max height statistics values
                calculatedHeightMin = points.getFirst()[1];
                calculatedHeightMax = points.getFirst()[1];
            } else if (type.equals("speed")) {
                // Update max speed statistics values
                calculatedSpeedMax = points.getFirst()[1];
            }
            return bmp;
        }

        if ((type.equals("height") && height_smoothing_window > 1) ||
                (type.equals("speed") && speed_smoothing_window > 1)) {
            // Calculate height/speed average with sliding window
            // Ignore some of the highest and lowest values to avoid outliers

            LinkedList<float[]> pointscopy = new LinkedList<>();
            for (int i = 0; i < points.size(); i++) {
                int width;
                if (type.equals("height")) {
                    width = height_smoothing_window / 2;
                } else {
                    width = speed_smoothing_window / 2;
                }

                int start = Math.max(0, i - width);
                int end = Math.min(i + width + 1, points.size());

                LinkedList<Float> window = new LinkedList<>();
                for (float[] p : points.subList(start, end)) {
                    window.add(p[1]);
                }
                Collections.sort(window);

                start = window.size() / 3;
                end = window.size() - start;
                List<Float> subWindow = window.subList(start, end);
                float avg = 0;
                for (float h : subWindow) {
                    avg += h;
                }
                float y = avg / subWindow.size();

                float[] pNew = {points.get(i)[0], y};
                pointscopy.add(pNew);
            }
            points = pointscopy;
        }

        // Calculate min and max for x and y axis
        float minX = Float.POSITIVE_INFINITY;
        float minY = Float.POSITIVE_INFINITY;
        float maxX = Float.NEGATIVE_INFINITY;
        float maxY = Float.NEGATIVE_INFINITY;
        for (float[] p : points) {
            if (p[0] < minX) {
                minX = p[0];
            }
            if (p[1] < minY) {
                minY = p[1];
            }
            if (p[0] > maxX) {
                maxX = p[0];
            }
            if (p[1] > maxY) {
                maxY = p[1];
            }
        }
        float diffX = maxX - minX;
        float diffY = maxY - minY;
        float diffMax = Math.max(diffX, diffY);
        float diffMin = Math.min(diffX, diffY);

        if (type.equals("height")) {
            // Update min and max height statistics values
            calculatedHeightMin = minY;
            calculatedHeightMax = maxY;
        } else if (type.equals("speed")) {
            // Update max speed statistics values
            calculatedSpeedMax = maxY;
        }

        if (diffMax < 0.00001 ||
                (diffY < 0.1 && (type.equals("height") || type.equals("speed")))) {
            // No need for following drawing calculations because all points are (almost) on the same spot
            canvas.drawCircle(sizeX / 2, sizeY / 2, pointRadius, paintGreen);
            return bmp;
        }

        if (type.equals("location")) {
            // Move the coordinates, so that the x center is at 0 -> Center the distortion
            // Create a new array list with the new coordinates to replace the other one
            LinkedList<float[]> pointscopy = new LinkedList<>();
            float centerX = (minX + maxX) / 2;
            for (float[] p : points) {
                float y = p[1];
                float x = p[0] - centerX;
                if (x > Math.PI * 2) {
                    x = x - (float) Math.PI * 2;
                }
                if (x < 0) {
                    x = x + (float) Math.PI * 2;
                }

                float sphericalDistortion = (float) Math.cos(y);
                x = (float) Math.sin(x);
                y = (float) Math.sin(y);
                x = x * sphericalDistortion;
                float[] pNew = {x, y};
                pointscopy.add(pNew);
            }
            points = pointscopy;

            // Calculate min and max for x and y axis again
            minX = Float.POSITIVE_INFINITY;
            minY = Float.POSITIVE_INFINITY;
            maxX = Float.NEGATIVE_INFINITY;
            maxY = Float.NEGATIVE_INFINITY;
            for (float[] p : points) {
                if (p[0] < minX) {
                    minX = p[0];
                }
                if (p[1] < minY) {
                    minY = p[1];
                }
                if (p[0] > maxX) {
                    maxX = p[0];
                }
                if (p[1] > maxY) {
                    maxY = p[1];
                }
            }
            diffX = maxX - minX;
            diffY = maxY - minY;
            diffMax = Math.max(Math.abs(diffX), Math.abs(diffY));
            diffMin = Math.min(Math.abs(diffX), Math.abs(diffY));
        }

        float lastX = 0;
        float lastY = 0;
        for (int i = 0; i < points.size(); i++) {
            float[] p = points.get(i);
            float x, y;

            if (type.equals("location")) {
                // Scale coordinates to [0,1] without changing ratio
                x = ((p[0] - minX) / diffMax);
                y = ((p[1] - minY) / diffMax) * Math.signum(diffY);

                // Center the smaller axis
                float c = ((diffMax - diffMin) / diffMax) / 2;
                if (diffX == diffMin) {
                    x = x + c;
                }
                if (Math.abs(diffY) == diffMin) {
                    y = y + c * Math.signum(diffY);
                }

            } else {
                // Scale coordinates to [0,1]
                x = ((p[0] - minX) / diffX);
                y = ((p[1] - minY) / diffY);
            }

            // Bottom-Left as 0|0 of bitmap
            y = 1 - y;

            // Scale coordinates to bitmap size (keep a margin of pointRadius on all sides)
            x = x * (sizeX - pointRadius * 2) + pointRadius;
            y = y * (sizeY - pointRadius * 2) + pointRadius;

            if (i == 0) {
                // Can't draw a line in this step
                if (type.equals("location")) {
                    canvas.drawCircle(x, y, pointRadius, paintGreen);
                }
            } else {
                if (!(lastX == x && lastY == y)) {
                    // Don't draw points or lines on the same spots

                    if (type.equals("location")) {
                        canvas.drawLine(lastX, lastY, x, y, paintGreen);
                        if (i == (points.size() - 1)) {
                            // Draw the current (last) location in white color
                            canvas.drawCircle(x, y, pointRadius, paintWhite);
                        } else {
                            canvas.drawCircle(x, y, pointRadius, paintGreen);
                        }
                    } else {
                        canvas.drawLine(lastX, lastY, x, y, paintGreen);
                    }
                } else {
                    if (i == (points.size() - 1) && type.equals("location")) {
                        // Draw the current (last) location in white color
                        // Draw even if there is a point earlier on the same spot
                        canvas.drawCircle(x, y, pointRadius, paintWhite);
                    }
                }
            }

            lastX = x;
            lastY = y;
        }

        return bmp;
    }

    static void clearGraphData(Context context) {
        locationPoints = new LinkedList<>();
        heightPoints = new LinkedList<>();
        speedPoints = new LinkedList<>();
        saveGraphData(context);
        updateGraphsAndTextsThreaded();
    }

    static void saveGraphData(Context context) {
        File dir = new File(context.getFilesDir() + "/graph_data/");
        if (!dir.isDirectory()) {
            boolean mkdir = dir.mkdir();
            if (!mkdir) {
                Log.e("GraphManager", "Could not create graph_data directory");
                return;
            }
        }

        File locationFile = new File(dir, "locationData.bin");
        try {
            FileOutputStream fos = new FileOutputStream(locationFile);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(locationPoints);
            oos.close();
            fos.close();
        } catch (IOException e) {
            boolean delete = locationFile.delete();
            if (!delete) {
                Log.e("GraphManager", "Could not delete corrupt file.");
            }
            e.printStackTrace();
        }

        File heightFile = new File(dir, "heightData.bin");
        try {
            FileOutputStream fos = new FileOutputStream(heightFile);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(heightPoints);
            oos.close();
            fos.close();
        } catch (IOException e) {
            boolean delete = heightFile.delete();
            if (!delete) {
                Log.e("GraphManager", "Could not delete corrupt file.");
            }
            e.printStackTrace();
        }

        File speedFile = new File(dir, "speedData.bin");
        try {
            FileOutputStream fos = new FileOutputStream(speedFile);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(speedPoints);
            oos.close();
            fos.close();
        } catch (IOException e) {
            boolean delete = speedFile.delete();
            if (!delete) {
                Log.e("GraphManager", "Could not delete corrupt file.");
            }
            e.printStackTrace();
        }
    }

    static void loadGraphDataThreaded(final Context context) {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                loadGraphData(context);
                updateGraphsAndTextsThreaded();
            }
        });
        t.start();
    }

    private static void loadGraphData(Context context) {
        File dir = new File(context.getFilesDir() + "/graph_data/");
        locationPoints = new LinkedList<>();
        heightPoints = new LinkedList<>();
        speedPoints = new LinkedList<>();

        if (dir.isDirectory()) {
            File locationFile = new File(dir, "locationData.bin");
            try {
                FileInputStream fis = new FileInputStream(locationFile);
                ObjectInputStream ois = new ObjectInputStream(fis);
                locationPoints = (LinkedList) ois.readObject();
                ois.close();
                fis.close();
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }

            File heightFile = new File(dir, "heightData.bin");
            try {
                FileInputStream fis = new FileInputStream(heightFile);
                ObjectInputStream ois = new ObjectInputStream(fis);
                heightPoints = (LinkedList) ois.readObject();
                ois.close();
                fis.close();
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }

            File speedFile = new File(dir, "speedData.bin");
            try {
                FileInputStream fis = new FileInputStream(speedFile);
                ObjectInputStream ois = new ObjectInputStream(fis);
                speedPoints = (LinkedList) ois.readObject();
                ois.close();
                fis.close();
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }

            updateGraphsAndTextsThreaded();
        }
    }

    static void update_thresholds(String key, int val) {
        switch (key) {
            case "stats_accuracy":
                accuracy_threshold_stats = val;
                break;
            case "height_accuracy":
                accuracy_threshold_height = val;
                break;
            case "height_smoothing":
                height_smoothing_window = val;
                updateGraphsAndTextsThreaded();
                break;
            case "speed_smoothing":
                speed_smoothing_window = val;
                updateGraphsAndTextsThreaded();
                break;
            default:
                throw new IllegalArgumentException();
        }
    }
}
