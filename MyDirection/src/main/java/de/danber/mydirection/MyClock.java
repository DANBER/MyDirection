package de.danber.mydirection;

import android.os.Handler;

import static java.lang.Thread.sleep;

public class MyClock implements Runnable {
    private final Object locker;
    private boolean paused;
    private ServiceGPS serviceGPS;
    private Handler handler;

    MyClock(ServiceGPS serviceGPS) {
        locker = new Object();
        paused = false;
        handler = new Handler();
        this.serviceGPS = serviceGPS;
    }

    public void run() {
        while (true) {

            handler.post(new Runnable() {
                public void run() {
                    serviceGPS.onTimeChanged();
                }
            });

            try {
                sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            synchronized (locker) {
                while (paused) {
                    try {
                        locker.wait();
                    } catch (Exception ignored) {
                    }
                }
            }
        }
    }

    void onPause() {
        synchronized (locker) {
            paused = true;
        }
    }

    void onResume() {
        synchronized (locker) {
            paused = false;
            locker.notifyAll();
        }
    }
}
