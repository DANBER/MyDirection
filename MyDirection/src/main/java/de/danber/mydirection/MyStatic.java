package de.danber.mydirection;

import android.content.res.Resources;
import android.graphics.Paint;
import android.graphics.Rect;
import android.location.Location;
import android.util.DisplayMetrics;

import java.math.BigDecimal;
import java.math.RoundingMode;

class MyStatic {
    static final int MY_PERMISSIONS_REQUEST_GPS_AND_EXTERN = 12;

    static double calcDistance(double B1, double L1, double B2, double L2) {
        float[] result = new float[3];
        Location.distanceBetween(B1, L1, B2, L2, result);
        return result[0] / 1000;
    }

    static String calcDistanceString(double B1, double L1, double B2, double L2) {
        double dist = calcDistance(B1, L1, B2, L2);
        BigDecimal bd = new BigDecimal(dist).setScale(2, RoundingMode.HALF_UP);
        return bd.toString();
    }

    static int resize(String string, double scale) {
        DisplayMetrics metrics = Resources.getSystem().getDisplayMetrics();
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        Rect bounds = new Rect();
        double flaeche = (metrics.heightPixels - 50 * metrics.density) * metrics.widthPixels / metrics.density / metrics
                .density / scale;
        int size = 1;

        paint.setTextSize(size);
        paint.getTextBounds(string, 0, string.length(), bounds);

        while (flaeche > bounds.width() * bounds.height()) {
            size++;
            paint.setTextSize(size);
            paint.getTextBounds(string, 0, string.length(), bounds);
        }
        return size - 1;
    }
}
