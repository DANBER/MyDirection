package de.danber.mydirection;

import androidx.annotation.NonNull;

public class Ziel implements Comparable<Ziel> {
    private Double nord, ost;
    private String name;

    Ziel(String name, String nord, String ost) throws MyException {
        setName(name);
        setNord(nord);
        setOst(ost);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Ziel) {
            Ziel ziel = (Ziel) obj;
            return (ziel.getNord() == nord && ziel.getOst() == ost);
        }
        return false;
    }

    @NonNull
    @Override
    public String toString() {
        return getName() + ": " + getNord() + " | " + getOst();
    }

    double getNord() {
        return nord;
    }

    void setNord(String nord) throws MyException {
        try {
            this.nord = (((int) (Double.parseDouble(nord) * 100000)) / 100000.0 + 180) % 360 - 180;
            if (Math.abs(this.nord) > 90) {
                this.nord = 180 * Math.signum(this.nord) - this.nord;
            }
        } catch (Exception e) {
            throw new MyException("Add a correct Latitude");
        }
    }

    double getOst() {
        return ost;
    }

    void setOst(String ost) throws MyException {
        try {
            this.ost = (((int) (Double.parseDouble(ost) * 100000)) / 100000.0 + 180) % 360 - 180;
        } catch (Exception e) {
            throw new MyException("Add a correct Longitude");
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) throws MyException {
        if (name != null && !name.equals("")) {
            if (name.matches("^([a-zA-Z0-9]+([ -_][a-zA-Z0-9]+)*)$")) {
                this.name = name;
            } else {
                throw new MyException("Name not allowed");
            }
        } else {
            throw new MyException("Add a Name");
        }
    }

    /**
     * Order by name alphabetically, using upper case letters
     */
    @Override
    public int compareTo(Ziel o) {
        String my_name = this.name.toUpperCase();
        String other_name = o.getName().toUpperCase();
        return my_name.compareTo(other_name);
    }
}