package de.danber.mydirection;

import android.Manifest;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

public class ServiceGPS extends Service implements LocationListener {
    private static LocationManager locationManager = null;
    private final String ServiceChannelId = "ChannelServiceGPS";
    private final IBinder iBinder = new LocalBinder();
    private MyClock clock;
    private int oldTime;
    private ServiceCallbacks serviceCallbacks = null;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        Log.w("ServiceGPS", "Started service");

        Intent notificationIntent = new Intent(this, ActivityMain.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);

        NotificationChannel serviceChannel = new NotificationChannel(
                ServiceChannelId,
                "Foreground Service Channel",
                NotificationManager.IMPORTANCE_LOW
        );
        NotificationManager manager = getSystemService(NotificationManager.class);
        manager.createNotificationChannel(serviceChannel);

        Notification notification = new Notification.Builder(this, ServiceChannelId)
                .setContentTitle(getText(R.string.app_name) + " " + getText(R.string.service_is_active))
                .setColor(getResources().getColor(R.color.green))
                .setVisibility(Notification.VISIBILITY_PUBLIC)
                .setContentText(getText(R.string.service_description))
                // Used https://romannurik.github.io/AndroidAssetStudio/icons-notification.html to generate icons for automatic coloring
                .setSmallIcon(R.drawable.icon_notification)
                .setContentIntent(pendingIntent)
                .setShowWhen(false)
                .setOngoing(true)
                .build();

        startForeground(1, notification);

        // Tells the system to not try to recreate the service after it has been killed.
        return START_NOT_STICKY;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        registerGPS();
        startClock();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        // Stop and delete clock as it sometimes continues running
        // and there were two clocks after the app was closed and opened again
        clock.onPause();
        clock = null;
        // Same for location listener
        if (locationManager != null) {
            locationManager.removeUpdates(this);
            locationManager = null;
        }

        Log.w("ServiceGPS", "Destroyed service");
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return iBinder;
    }

    @Override
    public void onLocationChanged(Location location) {
        CurrentLocation.setLocation(location);
        if (!isPaused()) {
            StatsManager.updateStats();
            GraphManager.onLocationChanged();
        } else {
            StatsManager.setOldLocation(CurrentLocation.getLatitude(), CurrentLocation.getLongitude());
        }

        if (serviceCallbacks != null) {
            serviceCallbacks.onLocationChanged();
        }
    }

    public void registerGPS() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

            SharedPreferences pref = getApplication().getSharedPreferences("SETTINGS", 0);
            int minTime = pref.getInt("UPDATE_INTERVAL", 2);

            if (locationManager == null) {
                locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
            } else {
                locationManager.removeUpdates(this);
            }
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, minTime * 1000, 0, this);
        }
    }

    private void startClock() {
        clock = new MyClock(this);
        Thread t = new Thread(clock);
        t.start();
        if (isPaused()) {
            clock.onPause();
        }
    }

    public void onTimeChanged() {
        StatsManager.increaseDuration();

        if (serviceCallbacks != null) {
            serviceCallbacks.onTimeChanged();
        }

        // Save stats every minute
        int nowTime = StatsManager.getDurationMinutes();
        if (oldTime != nowTime) {
            StatsManager.saveOldLocation(getApplication());
            StatsManager.saveStats(getApplication());
            GraphManager.saveGraphData(this.getBaseContext());
            oldTime = nowTime;
            Log.i("ServiceGPS", "Automatically saved statistics");
        }
    }

    public boolean isPaused() {
        SharedPreferences pref = getApplication().getSharedPreferences("STATS", 0);
        boolean paused = pref.getBoolean("PAUSED", false);
        return paused;
    }

    public void updateClock() {
        if (isPaused()) {
            clock.onPause();
        } else {
            clock.onResume();
        }
    }

    public void stopLocationUpdates() {
        if (locationManager != null) {
            locationManager.removeUpdates(this);
        }
    }

    public void updateRequestRate() {
        registerGPS();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {
        CurrentLocation.setLocation(null);
        if (serviceCallbacks != null) {
            serviceCallbacks.onProviderDisabled();
        }
    }

    public void setCallbacks(ServiceCallbacks callbacks) {
        serviceCallbacks = callbacks;
    }

    public class LocalBinder extends Binder {
        /**
         * Returns the instance of the service
         */
        public ServiceGPS getService() {
            return ServiceGPS.this;
        }
    }
}
