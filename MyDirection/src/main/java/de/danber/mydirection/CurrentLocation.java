package de.danber.mydirection;

import android.location.Location;

import java.math.BigDecimal;
import java.math.RoundingMode;

class CurrentLocation {
    private static Location location;
    private static Double altitude;

    static void setLocation(Location loc) {
        location = loc;
        if (location != null) {
            setAltitude(location.getAltitude());
        }
    }

    static boolean hasLocation() {
        return location != null;
    }

    static double getLatitude() {
        return location.getLatitude();
    }

    static double getLongitude() {
        return location.getLongitude();
    }

    static Double getAltitude() {
        return altitude;
    }

    private static void setAltitude(Double alt) {
        if (EGM96GRID.isGridLoaded()) {
            altitude = alt - EGM96GRID.getCorrection(getLatitude(), getLongitude());
        } else {
            altitude = null;
        }
    }

    static double getSpeed() {
        return location.getSpeed() * 3.6;
    }

    static double getAccuracy() {
        return location.getAccuracy();
    }

    static String getLatitudeString() {
        return Location.convert(getLatitude(), Location.FORMAT_DEGREES);
    }

    static String getLongitudeString() {
        return Location.convert(getLongitude(), Location.FORMAT_DEGREES);
    }

    static String getSpeedString() {
        BigDecimal bd = new BigDecimal(getSpeed()).setScale(1, RoundingMode.HALF_UP);
        return bd.toString();
    }

    static String getAltitudeString() {
        BigDecimal bd = new BigDecimal(getAltitude()).setScale(0, RoundingMode.HALF_UP);
        return bd.toString();
    }

    static String getAccuracyString() {
        BigDecimal bd = new BigDecimal(getAccuracy()).setScale(1, RoundingMode.HALF_UP);
        return bd.toString();
    }
}
