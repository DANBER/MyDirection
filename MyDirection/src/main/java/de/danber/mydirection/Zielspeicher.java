package de.danber.mydirection;

import android.content.Context;
import android.util.Log;
import android.util.Xml;

import androidx.annotation.NonNull;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xmlpull.v1.XmlSerializer;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

public class Zielspeicher implements Iterable<Ziel> {
    private static Zielspeicher unique = null;
    private List<Ziel> zielliste = new ArrayList<>();

    private Zielspeicher() {
    }

    static Zielspeicher instance() {
        if (unique == null) {
            unique = new Zielspeicher();
        }
        return unique;
    }

    static Ziel read(File file, InputStream inputStream) throws MyException {
        Ziel ziel = null;
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();

            Document doc;
            if (file != null) {
                doc = builder.parse(file);
                Log.d("Zielspeicher", "Loading destination: " + file.getAbsolutePath());
            } else {
                doc = builder.parse(inputStream);
            }
            doc.getDocumentElement().normalize();

            Node node = doc.getDocumentElement().getElementsByTagName("wpt").item(0);
            Element element = null;
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                element = (Element) node;
            }

            if (element != null) {
                ziel = new Ziel("Point", "0", "0");
                ziel.setName(element.getElementsByTagName("name").item(0).getTextContent());
                ziel.setNord(doc.getDocumentElement().getElementsByTagName("wpt").item(0).
                        getAttributes().getNamedItem("lat").getNodeValue());
                ziel.setOst(doc.getDocumentElement().getElementsByTagName("wpt").item(0).
                        getAttributes().getNamedItem("lon").getNodeValue());
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new MyException("File not readable");
        }
        return ziel;
    }

    void addAndSave(Context context, Ziel ziel) throws MyException {
        for (Ziel z : zielliste) {
            if (z.equals(ziel)) {
                throw new MyException("Location already saved");
            }
        }
        if (!isNameUnique(ziel)) {
            throw new MyException("Name already used");
        }
        zielliste.add(ziel);
        save(context, ziel);

        // Sort the list
        Collections.sort(zielliste);
    }

    private boolean isNameUnique(Ziel ziel) {
        for (Ziel z : zielliste) {
            if (z.getName().equals(ziel.getName())) {
                return false;
            }
        }
        return true;
    }

    Ziel get(int pos) {
        return zielliste.get(pos);
    }

    List<Ziel> gibListe() {
        return zielliste;
    }

    @NonNull
    @Override
    public Iterator<Ziel> iterator() {
        return zielliste.iterator();
    }

    void load(Context context) throws MyException {
        File dir = new File(context.getFilesDir() + "/destinations/");

        if (dir.isDirectory()) {
            zielliste.clear();
            if (dir.list() != null) {
                for (String path : Objects.requireNonNull(dir.list())) {
                    File file = new File(dir, path);
                    zielliste.add(read(file, null));
                }
            } else {
                Log.d("Zielspeicher", "No saved destinations existing");
            }

            // Sort the list
            Collections.sort(zielliste);
        }
    }

    static void save(String path, Ziel ziel) {
        File file = new File(path);
        File dir = file.getParentFile();

        if (!dir.isDirectory()) {
            boolean mkdir = dir.mkdirs();
            if (!mkdir) {
                Log.e("Zielspeicher", "Could not create destinations directory");
                return;
            }
        }

        try {
            FileOutputStream fos = new FileOutputStream(file);

            XmlSerializer serializer = Xml.newSerializer();
            serializer.setOutput(fos, "UTF-8");
            serializer.startDocument(null, true);
            serializer.setFeature("http://xmlpull.org/v1/doc/features.html#indent-output", true);
            serializer.startTag(null, "gpx");
            serializer.attribute(null, "version", "1.0");

            serializer.startTag(null, "name");
            serializer.text("Destination: " + ziel.getName());
            serializer.endTag(null, "name");

            serializer.startTag(null, "wpt");
            serializer.attribute(null, "lat", "" + ziel.getNord());
            serializer.attribute(null, "lon", "" + ziel.getOst());

            serializer.startTag(null, "name");
            serializer.text(ziel.getName());
            serializer.endTag(null, "name");

            serializer.endDocument();
            serializer.flush();
            fos.close();
            Log.d("Zielspeicher", "Saved destination: " + path);
        } catch (IOException e) {
            boolean delete = file.delete();
            if (!delete) {
                Log.e("Zielspeicher", "Could not delete corrupt file." + path);
            }
            e.printStackTrace();
        }
    }

    static void delete(String path) {
        File file = new File(path);
        if (file.exists()) {
            boolean delete = file.delete();
            if (!delete) {
                Log.d("Zielspeicher", "Deleted destination: " + path);
            } else {
                Log.e("Zielspeicher", "Could not delete destination: " + path);
            }
        }
    }

    private void save(Context context, Ziel ziel) {
        String filename = context.getFilesDir() + "/destinations/" + ziel.getName() + ".gpx";
        save(filename, ziel);
    }

    void delete(Context context, Ziel ziel) {
        String filename = context.getFilesDir() + "/destinations/" + ziel.getName() + ".gpx";
        zielliste.remove(ziel);
        delete(filename);
    }
}