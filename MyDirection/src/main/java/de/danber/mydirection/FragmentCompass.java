package de.danber.mydirection;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

public class FragmentCompass extends Fragment implements SensorEventListener {
    private SensorManager sensorManager;
    private ActivityMain macy;

    private double ZielB = 0;
    private double ZielL = 0;
    private double PunktB = 0;
    private double PunktL = 0;
    private boolean Position = false;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        View root = inflater.inflate(R.layout.fragment_compass, container, false);
        macy = ((ActivityMain) getActivity());
        initialize(root);

        if (CurrentLocation.hasLocation()) {
            onLocationChanged(macy);
        } else {
            updateStatsText(macy);
        }
        onTimeChanged(macy);

        return root;
    }

    private void initialize(View root) {

        sensorManager = (SensorManager) macy.getSystemService(Context.SENSOR_SERVICE);
        if (sensorManager != null) {
            sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE), SensorManager.SENSOR_DELAY_NORMAL);
        }

        // Update play/pause button image and add click listener
        ImageView iv = root.findViewById(R.id.button_play_pause);
        if (macy.isPaused()) {
            iv.setImageResource(R.drawable.play_button);
        }
        iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                macy.animation(v);
                if (macy.isPaused()) {
                    macy.savePlayPause(false);
                    ((ImageView) v).setImageResource(R.drawable.pause_button);
                } else {
                    macy.savePlayPause(true);
                    ((ImageView) v).setImageResource(R.drawable.play_button);
                }
            }
        });
        if (macy.getSharedPreferences("SETTINGS", 0).getString("DISPLAY_PAUSE", "on").equals("off")) {
            // Unpause tracking before hiding the button, hiding overrides pausing
            iv.setImageResource(R.drawable.pause_button);
            macy.savePlayPause(false);
            iv.setVisibility(View.GONE);
        }

        loadZiel();
    }

    private void loadZiel() {
        SharedPreferences pref = macy.getSharedPreferences("DIRECTION", 0);
        ZielB = (double) pref.getFloat("LATITUDE", 0);
        ZielL = (double) pref.getFloat("LONGITUDE", 0);
    }

    void reload() {
        clearSpeedDistanceText();
        loadZiel();
        updateStatsText(macy);
        onTimeChanged(macy);
    }

    void onLocationChanged(ActivityMain macy) {
        double altB = PunktB;
        double altL = PunktL;
        PunktB = CurrentLocation.getLatitude();
        PunktL = CurrentLocation.getLongitude();

        if (Position) {
            double d = winkel(PunktB, PunktL, ZielB, ZielL) - winkel(altB, altL, PunktB, PunktL);
            drehen(macy, d);
        }

        TextView tv = macy.findViewById(R.id.text_gps);
        if (tv != null) {
            if (MyStatic.calcDistance(PunktB, PunktL, ZielB, ZielL) < 0.01) {
                tv.setText(getString(R.string.arrived));
            } else if (CurrentLocation.getSpeed() < 1) {
                tv.setText(getString(R.string.too_slow));
            } else {
                tv.setText("");
            }
        }

        tv = macy.findViewById(R.id.text_speed);
        if (tv != null) {
            String str = CurrentLocation.getSpeedString() + " KMH";
            tv.setText(str);
        }

        tv = macy.findViewById(R.id.text_distance);
        if (tv != null) {
            String str = MyStatic.calcDistanceString(PunktB, PunktL, ZielB, ZielL) + " KM";
            tv.setText(str);
        }

        updateStatsText(macy);
        Position = true;
    }

    private void clearSpeedDistanceText() {
        ((TextView) macy.findViewById(R.id.text_speed)).setText(macy.getString(R.string.default_speed_value));
        ((TextView) macy.findViewById(R.id.text_distance)).setText(macy.getString(R.string.default_distance_value));
    }

    private void updateStatsText(ActivityMain macy) {
        TextView tv = macy.findViewById(R.id.text_way);
        if (tv != null) {
            String str = StatsManager.getTravelledString() + " KM";
            tv.setText(str);
        }
    }

    void onTimeChanged(ActivityMain macy) {
        TextView tv = macy.findViewById(R.id.text_time);
        if (tv != null) {
            tv.setText(StatsManager.getDurationFormatted());
        }
    }

    void onProviderDisabled(ActivityMain macy) {
        TextView tv = macy.findViewById(R.id.text_gps);
        if (tv != null) {
            tv.setText(getString(R.string.wait_gps));
        }
        Position = false;
    }

    private double winkel(double B1, double L1, double B2, double L2) {
        Location loc = new Location("");
        loc.setLatitude(B1);
        loc.setLongitude(L1);
        Location loz = new Location("");
        loz.setLatitude(B2);
        loz.setLongitude(L2);
        return loc.bearingTo(loz);
    }

    private void drehen(ActivityMain macy, double d) {
        ImageView iv = macy.findViewById(R.id.image_arrow);
        if (iv != null && !Double.isNaN(d)) {
            Bitmap bmp = BitmapFactory.decodeResource(macy.getResources(), R.drawable.richtungspfeil);
            Matrix matrix = new Matrix();
            matrix.postRotate((float) d);
            Bitmap rotated = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight(), matrix, true);
            iv.setImageBitmap(rotated);
        }
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (!Position) {
            float degree = Math.round(event.values[2]);
            drehen(macy, degree);
        } else {
            sensorManager.unregisterListener(this);
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }
}