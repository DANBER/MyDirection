package de.danber.mydirection;

public interface ServiceCallbacks {
    void onLocationChanged();

    void onTimeChanged();

    void onProviderDisabled();
}
