package de.danber.mydirection;

import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.text.HtmlCompat;
import androidx.fragment.app.Fragment;

public class FragmentHelp extends Fragment {

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        View root = inflater.inflate(R.layout.fragment_help, container, false);
        initialize(root);
        return root;
    }

    private void initialize(View root) {
        TextView tv_i = root.findViewById(R.id.text_info);
        tv_i.setText(HtmlCompat.fromHtml(getString(R.string.info), HtmlCompat.FROM_HTML_MODE_LEGACY));
        tv_i.setTextSize(MyStatic.resize(getResources().getString(R.string.info), 12));
        tv_i.setMovementMethod(LinkMovementMethod.getInstance());

        TextView tv_s = root.findViewById(R.id.text_source);
        tv_s.setText(HtmlCompat.fromHtml(getString(R.string.source_link), HtmlCompat.FROM_HTML_MODE_LEGACY));
        tv_s.setTextSize(MyStatic.resize(getResources().getString(R.string.info), 12));
        tv_s.setMovementMethod(LinkMovementMethod.getInstance());
    }
}