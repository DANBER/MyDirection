package de.danber.mydirection;

import android.app.Application;
import android.content.SharedPreferences;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.TimeZone;

class StatsManager {
    private static float duration;
    private static float travelled;

    private static boolean oldLocSet = false;
    private static float oldLatitude;
    private static float oldLongitude;
    private static int accuracy_threshold_stats;

    static void updateStats() {
        if (CurrentLocation.getAccuracy() > accuracy_threshold_stats) {
            return;
        }

        if (oldLocSet) {
            travelled += MyStatic.calcDistance(oldLatitude, oldLongitude, CurrentLocation.getLatitude(), CurrentLocation.getLongitude());
        }

        setOldLocation(CurrentLocation.getLatitude(), CurrentLocation.getLongitude());
    }

    static void setOldLocation(double latitude, double longitude) {
        oldLatitude = (float) latitude;
        oldLongitude = (float) longitude;
        oldLocSet = true;
    }

    static void saveOldLocation(Application macy) {
        SharedPreferences pref = macy.getSharedPreferences("OLD_LOCATION", 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.putFloat("LATITUDE", oldLatitude);
        editor.putFloat("LONGITUDE", oldLongitude);
        editor.apply();
    }

    static void loadOldLocation(Application macy) {
        SharedPreferences pref = macy.getSharedPreferences("OLD_LOCATION", 0);
        oldLatitude = pref.getFloat("LATITUDE", -1000);
        oldLongitude = pref.getFloat("LONGITUDE", -1000);
        if (oldLatitude == -1000) {
            oldLocSet = false;
        } else {
            oldLocSet = true;
        }
    }

    static void clearOldLocation(Application macy) {
        SharedPreferences pref = macy.getSharedPreferences("OLD_LOCATION", 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.clear();
        editor.apply();
        oldLocSet = false;
    }

    static void saveStats(Application macy) {
        SharedPreferences pref = macy.getSharedPreferences("STATS", 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.putFloat("DURATION", duration);
        editor.putFloat("TRAVELLED", travelled);
        editor.apply();
    }

    static void loadStats(Application macy) {
        // Load thresholds
        SharedPreferences pref = macy.getSharedPreferences("SETTINGS", 0);
        accuracy_threshold_stats = pref.getInt("STATS_ACCURACY", 20);

        // Load statistics
        pref = macy.getSharedPreferences("STATS", 0);
        duration = pref.getFloat("DURATION", 0);
        travelled = pref.getFloat("TRAVELLED", 0);
    }

    static void clearStats(Application macy) {
        SharedPreferences pref = macy.getSharedPreferences("STATS", 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.clear();
        editor.apply();

        duration = 0;
        travelled = 0;
    }

    static void update_thresholds(String key, int val) {
        if (key.equals("stats_accuracy")) {
            accuracy_threshold_stats = val;
        } else {
            throw new IllegalArgumentException();
        }
    }

    static void increaseDuration() {
        duration += 1;
    }

    private static float getAvgSpeed() {
        float avgSpeed = duration == 0 ? 0 : travelled / (duration / 3600f);
        return avgSpeed;
    }

    static float getTravelled() {
        return travelled;
    }

    static String getDurationFormatted() {
        SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss", Locale.getDefault());
        df.setTimeZone(TimeZone.getTimeZone("UTC"));
        return String.valueOf(df.format(duration * 1000));
    }

    static int getDurationMinutes() {
        return (int) duration / 60;
    }

    static String getTravelledString() {
        BigDecimal bd = new BigDecimal(getTravelled()).setScale(2, RoundingMode.HALF_UP);
        return bd.toString();
    }

    static String getAvgSpeedString() {
        BigDecimal bd = new BigDecimal(getAvgSpeed()).setScale(1, RoundingMode.HALF_UP);
        return bd.toString();
    }
}
