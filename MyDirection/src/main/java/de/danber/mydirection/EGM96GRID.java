package de.danber.mydirection;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;

class EGM96GRID {
    private static int NUM_ROWS = 721;
    private static int NUM_COLS = 1440;
    private static short[][] EGMGrid = new short[NUM_COLS][NUM_ROWS];
    private static boolean gridLoaded = false;

    static double getCorrection(double lat, double lon) {
        if (!gridLoaded) {
            return Float.MIN_VALUE;
        }

        lat = 90.0 - lat;
        lon = (lon < 0) ? lon + 360.0 : lon;

        double INTERVAL = 0.25;
        int ilat = (int) (lat / INTERVAL);
        int ilon = (int) (lon / INTERVAL);

        short hp11 = EGMGrid[ilon][ilat];
        short hp12 = EGMGrid[ilon][(ilat + 1) % NUM_ROWS];
        short hp21 = EGMGrid[(ilon + 1) % NUM_COLS][ilat];
        short hp22 = EGMGrid[(ilon + 1) % NUM_COLS][(ilat + 1) % NUM_ROWS];

        // Interpolation:
        double hp1 = hp11 + (hp12 - hp11) * (lat % INTERVAL) / INTERVAL;
        double hp2 = hp21 + (hp22 - hp21) * (lat % INTERVAL) / INTERVAL;
        double hp = hp1 + (hp2 - hp1) * (lon % INTERVAL) / INTERVAL;
        return hp / 100;
    }

    static void runLoadThread(final ActivityMain macy) {
        Thread t = new Thread(new Runnable() {

            @Override
            public void run() {
                loadGrid(macy);
            }
        });
        t.start();
    }

    private static void loadGrid(ActivityMain macy) {
        try {
            InputStream is = macy.getAssets().open("files/WW15MGH.DAC");
            BufferedInputStream bis = new BufferedInputStream(is);
            DataInputStream dis = new DataInputStream(bis);

            for (int i_lat = 0; i_lat < NUM_ROWS; i_lat++) {
                for (int i_lon = 0; i_lon < NUM_COLS; i_lon++) {
                    Short s = dis.readShort();
                    EGMGrid[i_lon][i_lat] = s;
                }
            }
            gridLoaded = true;

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    static boolean isGridLoaded() {
        return gridLoaded;
    }
}
