package de.danber.mydirection;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

public class FragmentStats extends Fragment {


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        View root = inflater.inflate(R.layout.fragment_stats, container, false);
        ActivityMain macy = ((ActivityMain) getActivity());

        if (CurrentLocation.hasLocation()) {
            updateLocationText(macy);
        }

        updateStatsText(macy);
        onTimeChanged(macy);
        GraphManager.updateGraphsAndTextsThreaded();

        return root;
    }

    void onLocationChanged(ActivityMain macy) {
        updateLocationText(macy);
        if (!macy.isPaused()) {
            updateStatsText(macy);
        }
    }

    private void updateLocationText(ActivityMain macy) {
        TextView tv = macy.findViewById(R.id.text_daten_n1);
        if (tv != null) {
            tv.setText(CurrentLocation.getLatitudeString());
        }

        tv = macy.findViewById(R.id.text_daten_e1);
        if (tv != null) {
            tv.setText(CurrentLocation.getLongitudeString());
        }

        tv = macy.findViewById(R.id.text_daten_s1);
        if (tv != null) {
            tv.setText(CurrentLocation.getSpeedString());
        }

        tv = macy.findViewById(R.id.text_daten_a1);
        if (tv != null) {
            tv.setText(CurrentLocation.getAccuracyString());
        }

        if (CurrentLocation.getAltitude() != null) {
            tv = macy.findViewById(R.id.text_daten_h1);
            if (tv != null) {
                tv.setText(CurrentLocation.getAltitudeString());
            }
        }
    }

    void updateStatsText(ActivityMain macy) {
        TextView tv = macy.findViewById(R.id.text_daten_t1);
        if (tv != null) {
            tv.setText(StatsManager.getTravelledString());
        }

        tv = macy.findViewById(R.id.text_daten_as1);
        if (tv != null) {
            tv.setText(StatsManager.getAvgSpeedString());
        }
    }

    void onTimeChanged(ActivityMain macy) {
        TextView tv = macy.findViewById(R.id.text_daten_d1);
        if (tv != null) {
            tv.setText(StatsManager.getDurationFormatted());
        }

        tv = macy.findViewById(R.id.text_daten_as1);
        if (tv != null) {
            tv.setText(StatsManager.getAvgSpeedString());
        }
    }
}