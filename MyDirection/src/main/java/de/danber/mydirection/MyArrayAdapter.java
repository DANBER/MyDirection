package de.danber.mydirection;

import android.content.Context;
import android.location.Location;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;

import java.util.List;

public class MyArrayAdapter<Ziel> extends ArrayAdapter<Ziel> {
    private Context context;
    private Zielspeicher z = Zielspeicher.instance();

    MyArrayAdapter(Context context, List<Ziel> objects) {
        super(context, R.layout.listelement, objects);
        this.context = context;
    }

    @NonNull
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View row = inflater.inflate(R.layout.listelement, parent, false);

        TextView tv1 = row.findViewById(R.id.element);
        tv1.setText(z.get(position).getName());

        TextView tv2 = row.findViewById(R.id.element2);
        String str = "North: " + Location.convert(z.get(position).getNord(), Location.FORMAT_DEGREES);
        tv2.setText(str);

        TextView tv3 = row.findViewById(R.id.element3);
        str = "East: " + Location.convert(z.get(position).getOst(), Location.FORMAT_DEGREES);
        tv3.setText(str);

        return row;
    }
}