package de.danber.mydirection;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import java.util.Objects;

public class FragmentSettings extends Fragment {
    private ActivityMain macy;
    private View root;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        root = inflater.inflate(R.layout.fragment_settings, container, false);
        macy = ((ActivityMain) getActivity());

        initialize();
        return root;
    }

    private void initialize() {
        int[] views = {R.id.gps_update_interval_val,
                R.id.gps_stats_accuracy_val,
                R.id.gps_height_accuracy_val,
                R.id.settings_toggle_pause_val,
                R.id.settings_height_smoothing_val,
                R.id.settings_speed_smoothing_val};

        for (final int vid : views) {
            TextView tv = root.findViewById(vid);

            // Set saved values
            SharedPreferences pref = macy.getSharedPreferences("SETTINGS", 0);
            if (vid == R.id.gps_update_interval_val) {
                int val = pref.getInt("UPDATE_INTERVAL", 2);
                ((TextView) root.findViewById(vid)).setText(String.valueOf(val));
            } else if (vid == R.id.gps_stats_accuracy_val) {
                int val = pref.getInt("STATS_ACCURACY", 20);
                ((TextView) root.findViewById(vid)).setText(String.valueOf(val));
            } else if (vid == R.id.gps_height_accuracy_val) {
                int val = pref.getInt("HEIGHT_ACCURACY", 15);
                ((TextView) root.findViewById(vid)).setText(String.valueOf(val));
            } else if (vid == R.id.settings_toggle_pause_val) {
                String val = pref.getString("DISPLAY_PAUSE", "on");
                ((TextView) root.findViewById(vid)).setText(val);
            } else if (vid == R.id.settings_height_smoothing_val) {
                int val = pref.getInt("HEIGHT_SMOOTHING", 5);
                ((TextView) root.findViewById(vid)).setText(String.valueOf(val));
            } else if (vid == R.id.settings_speed_smoothing_val) {
                int val = pref.getInt("SPEED_SMOOTHING", 3);
                ((TextView) root.findViewById(vid)).setText(String.valueOf(val));
            }

            // Set click listener
            ViewGroup parent = (ViewGroup) tv.getParent();
            parent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (vid == R.id.settings_toggle_pause_val) {
                        toggleView(vid);
                    } else {
                        showUpdateAlert(vid);
                    }
                }
            });
        }
    }

    private void showUpdateAlert(final int vid) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(macy);
        final EditText input = new EditText(macy);
        input.setInputType(InputType.TYPE_CLASS_NUMBER);
        input.setTypeface(getResources().getFont(R.font.font_digital));
        input.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
        builder.setView(input);

        builder.setPositiveButton("Change",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Editable edt = input.getText();
                        if (edt != null && !edt.toString().equals("")) {
                            try {
                                int val = Integer.parseInt(edt.toString());
                                if (val < 1) {
                                    Toast.makeText(macy, "Value to small (min 1)", Toast.LENGTH_SHORT).show();
                                } else if (val > 100 && (vid == R.id.gps_stats_accuracy_val || vid == R.id.gps_height_accuracy_val)) {
                                    Toast.makeText(macy, "Value to large (max 100)", Toast.LENGTH_SHORT).show();
                                } else if (val > 11 && (vid == R.id.settings_height_smoothing_val || vid == R.id.settings_speed_smoothing_val)) {
                                    Toast.makeText(macy, "Value to large (max 11)", Toast.LENGTH_SHORT).show();
                                } else if (val > 30 && (vid == R.id.gps_update_interval_val)) {
                                    Toast.makeText(macy, "Value to large (max 30)", Toast.LENGTH_SHORT).show();
                                } else if (val % 2 == 0 && (vid == R.id.settings_height_smoothing_val || vid == R.id.settings_speed_smoothing_val)) {
                                    Toast.makeText(macy, "Value has to be uneven", Toast.LENGTH_SHORT).show();
                                } else {
                                    TextView tv = root.findViewById(vid);
                                    tv.setText(String.valueOf(val));
                                    call_settings_updated(vid, val);
                                }
                            } catch (NumberFormatException e) {
                                e.printStackTrace();
                                Toast.makeText(macy, "Can not convert value to number", Toast.LENGTH_SHORT).show();
                            }
                        }
                        macy.hideKeyboard(input);
                        dialog.cancel();
                    }
                });
        builder.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        macy.hideKeyboard(input);
                        dialog.cancel();
                    }
                });

        // Change font type of buttons
        final AlertDialog alertDialog = builder.create();
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                Button btnPositive = alertDialog.getButton(Dialog.BUTTON_POSITIVE);
                btnPositive.setTypeface(getResources().getFont(R.font.font_digital));
                btnPositive.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);

                Button btnNegative = alertDialog.getButton(Dialog.BUTTON_NEGATIVE);
                btnNegative.setTypeface(getResources().getFont(R.font.font_digital));
                btnNegative.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
            }
        });

        Objects.requireNonNull(alertDialog.getWindow()).addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
        alertDialog.show();
    }

    private void toggleView(int vid) {
        TextView tv = root.findViewById(vid);
        String val;

        if (tv.getText().equals("on")) {
            val = "off";
        } else {
            val = "on";
        }
        tv.setText(val);
        call_settings_updated(vid, val);
    }

    private void call_settings_updated(int vid, int val) {
        SharedPreferences pref = macy.getSharedPreferences("SETTINGS", 0);
        SharedPreferences.Editor editor = pref.edit();
        String key;

        if (vid == R.id.gps_update_interval_val) {
            key = "update_interval";
            editor.putInt("UPDATE_INTERVAL", val);
        } else if (vid == R.id.gps_stats_accuracy_val) {
            key = "stats_accuracy";
            editor.putInt("STATS_ACCURACY", val);
        } else if (vid == R.id.gps_height_accuracy_val) {
            key = "height_accuracy";
            editor.putInt("HEIGHT_ACCURACY", val);
        } else if (vid == R.id.settings_height_smoothing_val) {
            key = "height_smoothing";
            editor.putInt("HEIGHT_SMOOTHING", val);
        } else if (vid == R.id.settings_speed_smoothing_val) {
            key = "speed_smoothing";
            editor.putInt("SPEED_SMOOTHING", val);
        } else {
            throw new IllegalArgumentException();
        }

        editor.apply();
        macy.updateSettingsValue(key, val);
    }

    private void call_settings_updated(int vid, String val) {
        SharedPreferences pref = macy.getSharedPreferences("SETTINGS", 0);
        SharedPreferences.Editor editor = pref.edit();

        if (vid == R.id.settings_toggle_pause_val) {
            editor.putString("DISPLAY_PAUSE", val);
            ImageView iv = macy.findViewById(R.id.button_play_pause);
            if (val.equals("off")) {
                // Unpause tracking before hiding the button
                iv.setImageResource(R.drawable.pause_button);
                macy.savePlayPause(false);
                iv.setVisibility(View.GONE);
            } else {
                iv.setVisibility(View.VISIBLE);
            }
        }
        editor.apply();
    }
}