package de.danber.mydirection;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.util.Log;
import android.util.TypedValue;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;

import java.io.File;
import java.util.ArrayList;
import java.util.Objects;

public class FragmentLocations extends Fragment {
    private Zielspeicher zz = Zielspeicher.instance();
    private ActivityMain macy;
    private MyArrayAdapter<Ziel> adapter;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        View root = inflater.inflate(R.layout.fragment_locations, container, false);
        macy = ((ActivityMain) getActivity());

        loadDestinations();

        adapter = new MyArrayAdapter<>(macy.getApplicationContext(), zz.gibListe());
        ListView lv = root.findViewById(R.id.listView);
        registerForContextMenu(lv);
        lv.setAdapter(adapter);
        lv.setChoiceMode(AbsListView.CHOICE_MODE_MULTIPLE);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                macy.newDirection(zz.get(position).getNord(), zz.get(position).getOst());
            }
        });

        return root;
    }

    @Override
    public void onCreateContextMenu(@NonNull ContextMenu menu, @NonNull View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        MenuInflater inflater = macy.getMenuInflater();
        inflater.inflate(R.menu.locations_context_menu, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();

        if (item.getItemId() == R.id.menu_item_rename) {
            Ziel ziel = zz.get(info.position);
            showRename(ziel);
            updateAdapter();

            Log.i("Renamed", "" + info.position + " - " + ziel.getName());
            return true;
        }

        if (item.getItemId() == R.id.menu_item_share) {
            Intent intent = new Intent();
            intent.setType("text/xml");
            intent.setAction(Intent.ACTION_SEND);

            String filename = macy.getBaseContext().getFilesDir() + "/destinations/" + zz.get(info.position).getName() + ".gpx";
            File file = new File(filename);
            Uri uri = FileProvider.getUriForFile(macy, BuildConfig.APPLICATION_ID + ".fileprovider", file);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);  // Grant temporary read permission to the content URI
            intent.putExtra(Intent.EXTRA_STREAM, uri);
            startActivity(Intent.createChooser(intent, "Send with: "));

            Log.i("Shared", "" + info.position + " - " + filename);
            return true;
        }

        if (item.getItemId() == R.id.menu_item_delete) {
            Ziel ziel = zz.get(info.position);
            zz.delete(macy.getBaseContext(), ziel);
            updateAdapter();

            Log.i("Deleted", "" + info.position + " - " + ziel.getName());
            return true;
        }

        return false;
    }

    void updateAdapter() {
        adapter.notifyDataSetChanged();
    }

    private void loadDestinations() {
        SharedPreferences pref = macy.getSharedPreferences("START", 0);
        if (!pref.getBoolean("FIRSTSTART", false)) {
            loadExamplePositions();

            SharedPreferences.Editor editor = pref.edit();
            editor.putBoolean("FIRSTSTART", true);
            editor.apply();
        } else {
            try {
                zz.load(macy.getBaseContext());
            } catch (MyException e) {
                Toast.makeText(macy, e.getMessage(), Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
        }
    }

    private void loadExamplePositions() {
        Zielspeicher zielspeicher = Zielspeicher.instance();

        // Add some example destinations
        ArrayList<Ziel> ziele = new ArrayList<>();
        try {
            ziele.add(new Ziel("Oktoberfest", "48.13190", "11.54986"));
            ziele.add(new Ziel("Uni Augsburg", "48.33387", "10.8986"));
            ziele.add(new Ziel("Kaaba", "21.42253", "39.82399"));
            ziele.add(new Ziel("North Pole", "90.0", "0.0"));
            ziele.add(new Ziel("Mount Everest", "27.98744", "86.92548"));
            ziele.add(new Ziel("Liberty Statue", "40.68925", "-74.04669"));
            ziele.add(new Ziel("Angkor Wat", "13.41169", "103.86730"));
            ziele.add(new Ziel("Great Pyramid", "29.97924", "31.13201"));
            ziele.add(new Ziel("Uluru", "-25.34554", "131.02835"));
        } catch (MyException e) {
            e.printStackTrace();
        }

        for (Ziel z : ziele) {
            try {
                zielspeicher.addAndSave(macy.getBaseContext(), z);
            } catch (MyException e) {
                e.printStackTrace();
            }
        }
    }

    private void showRename(final Ziel old) {
        AlertDialog.Builder builder = new AlertDialog.Builder(macy);
        final EditText input = new EditText(macy);
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        input.setTypeface(getResources().getFont(R.font.font_digital));
        input.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
        builder.setView(input);

        builder.setPositiveButton("Change",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Editable nam = input.getText();
                        if (nam != null && !nam.toString().equals("")) {
                            try {
                                Ziel neu = new Ziel(nam.toString(), "" + old.getNord(), "" + old.getOst());
                                zz.delete(macy.getBaseContext(), old);
                                zz.addAndSave(macy.getBaseContext(), neu);
                                updateAdapter();
                            } catch (MyException e) {
                                Toast.makeText(macy, e.getMessage(), Toast.LENGTH_SHORT).show();
                                e.printStackTrace();
                            }
                        }
                        macy.hideKeyboard(input);
                        dialog.cancel();
                    }
                });
        builder.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        macy.hideKeyboard(input);
                        dialog.cancel();
                    }
                });

        // Change font type of buttons
        final AlertDialog alertDialog = builder.create();
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                Button btnPositive = alertDialog.getButton(Dialog.BUTTON_POSITIVE);
                btnPositive.setTypeface(getResources().getFont(R.font.font_digital));
                btnPositive.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);

                Button btnNegative = alertDialog.getButton(Dialog.BUTTON_NEGATIVE);
                btnNegative.setTypeface(getResources().getFont(R.font.font_digital));
                btnNegative.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
            }
        });

        Objects.requireNonNull(alertDialog.getWindow()).addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
        alertDialog.show();
    }
}