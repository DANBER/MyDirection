package de.danber.mydirection;

import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;

import java.io.File;
import java.util.List;
import java.util.Locale;

public class FragmentCreate extends Fragment {
    private Zielspeicher zielspeicher = Zielspeicher.instance();
    private View root;
    private ActivityMain macy;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        root = inflater.inflate(R.layout.fragment_create, container, false);
        macy = ((ActivityMain) getActivity());

        if (CurrentLocation.hasLocation()) {
            onLocationChanged(macy);
        }

        root.findViewById(R.id.button_save).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                macy.hideKeyboard(v);
                macy.animation(v);
                Ziel ziel = checkInput();
                if (ziel != null) {
                    if (!ziel.getName().equals("TempNameForDest")) {
                        save(ziel);
                    } else {
                        Toast.makeText(getActivity(), "Add a Name", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
        root.findViewById(R.id.button_navigate).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                macy.hideKeyboard(v);
                macy.animation(v);
                Ziel ziel = checkInput();
                if (ziel != null) {
                    reset();
                    macy.newDirection(ziel.getNord(), ziel.getOst());
                }
            }
        });
        root.findViewById(R.id.button_share).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                macy.hideKeyboard(v);
                macy.animation(v);
                Ziel ziel = checkInput();
                if (ziel != null) {
                    if (!ziel.getName().equals("TempNameForDest")) {
                        share(ziel);
                    } else {
                        Toast.makeText(getActivity(), "Add a Name", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });


        root.findViewById(R.id.options_constraint_layout).setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    macy.hideKeyboard(v);
                }
            }
        });

        return root;
    }

    private Ziel checkInput() {
        Ziel ziel = null;
        String name = "TempNameForDest";
        String latitude = "";
        String longitude = "";
        boolean setLatLon = false;

        // Get name if set
        Editable nam = ((EditText) root.findViewById(R.id.edit_name)).getText();
        if (nam != null && !nam.toString().equals("")) {
            name = nam.toString();
        }

        // Search for address if given
        Editable adr = ((EditText) root.findViewById(R.id.edit_address)).getText();
        if (adr != null && !adr.toString().equals("")) {
            macy.findViewById(R.id.searchingForAddressCircle).setVisibility(View.VISIBLE);
            try {
                // Search with geocoder doesn't need internet permission
                Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
                List<Address> addresses = geocoder.getFromLocationName(adr.toString(), 1);
                Log.d("FragmentCreate", "Searching Address: " + adr.toString());
                if (addresses != null && addresses.size() > 0) {
                    Address address = addresses.get(0);
                    latitude = "" + address.getLatitude();
                    longitude = "" + address.getLongitude();
                    setLatLon = true;
                } else {
                    Toast.makeText(getActivity(), getString(R.string.error_find_address), Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                Toast.makeText(getActivity(), getString(R.string.error_find_address), Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
            macy.findViewById(R.id.searchingForAddressCircle).setVisibility(View.GONE);
        }

        if (!setLatLon) {
            // Get latitude and longitude from inputs
            Editable lat = ((EditText) root.findViewById(R.id.edit_breite)).getText();
            Editable lon = ((EditText) root.findViewById(R.id.edit_laenge)).getText();
            if (lat != null && !lat.toString().equals("") && lon != null && !lon.toString().equals("")) {
                latitude = lat.toString();
                longitude = lon.toString();
                setLatLon = true;
            }
        }

        if (!setLatLon) {
            if (CurrentLocation.hasLocation()) {
                latitude = "" + CurrentLocation.getLatitude();
                longitude = "" + CurrentLocation.getLongitude();
                setLatLon = true;
            }
        }

        if (setLatLon) {
            try {
                ziel = new Ziel(name, latitude, longitude);
            } catch (MyException e) {
                Toast.makeText(macy, e.getMessage(), Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
        } else {
            Toast.makeText(getActivity(), getString(R.string.error_values_missing), Toast.LENGTH_SHORT).show();
        }

        return ziel;
    }

    private void reset() {
        EditText nam = root.findViewById(R.id.edit_name);
        EditText lat = root.findViewById(R.id.edit_breite);
        EditText lon = root.findViewById(R.id.edit_laenge);
        EditText adr = root.findViewById(R.id.edit_address);

        nam.setText(null);
        lat.setText(null);
        lon.setText(null);
        adr.setText(null);
        nam.clearFocus();
        lat.clearFocus();
        lon.clearFocus();
        adr.clearFocus();

        InputMethodManager imm = (InputMethodManager) macy.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.hideSoftInputFromWindow((nam.getWindowToken()), 0);
        }
    }

    private void save(Ziel ziel) {
        try {
            zielspeicher.addAndSave(macy.getBaseContext(), ziel);
            reset();
            macy.updateLocationList();
            macy.setFragment(2);
        } catch (MyException e) {
            Toast.makeText(macy, e.getMessage(), Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        } catch (Exception e) {
            Toast.makeText(macy, getString(R.string.error_save_location), Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    private void share(Ziel ziel) {
        String filename = macy.getBaseContext().getCacheDir() + "/" + ziel.getName() + ".gpx";

        // Create temporary file to share
        Zielspeicher.save(filename, ziel);

        Intent intent = new Intent();
        intent.setType("text/xml");
        intent.setAction(Intent.ACTION_SEND);

        filename = macy.getBaseContext().getCacheDir() + "/" + ziel.getName() + ".gpx";
        File file = new File(filename);
        Uri uri = FileProvider.getUriForFile(macy, BuildConfig.APPLICATION_ID + ".fileprovider", file);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);  // Grant temporary read permission to the content URI
        intent.putExtra(Intent.EXTRA_STREAM, uri);
        startActivity(Intent.createChooser(intent, "Send with: "));

        // Delete temporary file again
        file.deleteOnExit();
    }

    void onLocationChanged(ActivityMain macy) {
        TextView tv = macy.findViewById(R.id.text_standpunkt);
        if (tv != null) {
            String n = CurrentLocation.getLatitudeString();
            String e = CurrentLocation.getLongitudeString();
            String s = String.format("%s N   %s E", n, e);
            tv.setText(s);
        }
    }
}