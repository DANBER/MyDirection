package de.danber.mydirection;

class MyException extends Exception {
    MyException(String error) {
        super(error);
    }
}