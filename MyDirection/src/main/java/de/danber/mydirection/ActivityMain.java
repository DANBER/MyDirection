package de.danber.mydirection;

import android.Manifest;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Objects;

public class ActivityMain extends FragmentActivity implements ServiceCallbacks, ActionBar.TabListener {

    private ViewPager viewPager;
    private MyFragmentPagerAdapter myFragmentPagerAdapter;
    private ActionBar actionBar;

    private ServiceConnection serviceConnection;
    private ServiceGPS serviceGPS;

    private FragmentCompass fragmentCompass;
    private FragmentStats fragmentStats;
    private FragmentLocations fragmentLocations;
    private FragmentCreate fragmentCreate;
    private FragmentSettings fragmentSettings;
    private FragmentHelp fragmentHelp;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initWindowBehavior();

        EGM96GRID.runLoadThread(this);
        StatsManager.loadStats(getApplication());
        StatsManager.loadOldLocation(this.getApplication());
        GraphManager.initialize(this);
        GraphManager.loadGraphDataThreaded(this.getBaseContext());

        initViewPageAdapter();
        askPermissions();
        createServiceConnection();

        // Start service
        final Intent intent = new Intent(this.getApplication(), ServiceGPS.class);
        this.getApplication().startForegroundService(intent);
    }

    @Override
    public void onStart() {
        super.onStart();

        // Bind to Service
        Intent intent = new Intent(this, ServiceGPS.class);
        bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);

        checkOpenIntent();
        if (serviceGPS != null) {
            serviceGPS.updateRequestRate();
        }

        // Refresh for points collected while app was not in foreground
        GraphManager.updateGraphsAndTextsThreaded();
    }

    @Override
    public void onStop() {
        super.onStop();

        if (isPaused()) {
            // There is no need to receive location updates if tracking is paused and activity not in foreground
            // So stop service to save power
            serviceGPS.stopLocationUpdates();
        }

        // Unbind from Service
        serviceGPS.setCallbacks(null);
        unbindService(serviceConnection);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        // Save statistics
        StatsManager.saveOldLocation(getApplication());
        StatsManager.saveStats(getApplication());
        GraphManager.saveGraphData(this.getBaseContext());

        // Stop service
        final Intent intent = new Intent(this.getApplication(), ServiceGPS.class);
        this.getApplication().stopService(intent);
    }

    /**
     * Keep screen on. Hide status and navigation bar.
     */
    private void initWindowBehavior() {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_ALLOW_LOCK_WHILE_SCREEN_ON);
        getWindow().getDecorView().setOnSystemUiVisibilityChangeListener
                (new View.OnSystemUiVisibilityChangeListener() {
                    @Override
                    public void onSystemUiVisibilityChange(int visibility) {
                        getWindow().getDecorView().setSystemUiVisibility(
                                View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
                    }
                });
    }

    /**
     * Create fragment pager adapter.
     */
    private void initViewPageAdapter() {
        viewPager = findViewById(R.id.pager);
        viewPager.setOffscreenPageLimit(5);
        myFragmentPagerAdapter = new MyFragmentPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(myFragmentPagerAdapter);

        actionBar = getActionBar();
        if (actionBar != null) {
            actionBar.setDisplayShowHomeEnabled(false);
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayUseLogoEnabled(false);
            actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        }

        String[] tabs = {getString(R.string.tab_compass), getString(R.string.tab_stats), getString(R.string.tab_locations), getString(R.string.tab_create), getString(R.string.tab_settings), getString(R.string.tab_help)};
        for (String name : tabs) {
            TextView t = new TextView(this);
            t.setText(name);
            t.setTypeface(getResources().getFont(R.font.font_digital));
            t.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.green));
            ActionBar.Tab tab = actionBar.newTab().setCustomView(t).setTabListener(this);
            actionBar.addTab(tab);
        }

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                actionBar.setSelectedNavigationItem(position);
                ((InputMethodManager) Objects.requireNonNull(getSystemService(Context.INPUT_METHOD_SERVICE)))
                        .hideSoftInputFromWindow(viewPager.getWindowToken(), 0);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
    }

    /**
     * Ask for gps permission if not given.
     */
    public void askPermissions() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, MyStatic.MY_PERMISSIONS_REQUEST_GPS_AND_EXTERN);
        }
    }

    /**
     * Hide status and navigation bar again if shown.
     */
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
            );
        }
    }

    /**
     * Handles intent created by opening file.gpx outside the app.
     */
    private void checkOpenIntent() {
        if (this.getIntent() != null && this.getIntent().getData() != null) {
            Uri uri = this.getIntent().getData();

            try {
                InputStream is = this.getContentResolver().openInputStream(uri);
                Ziel z = Zielspeicher.read(null, is);
                if (z != null) {
                    Log.d("ActivityMain", "Opened external destination: " + z.toString());
                    showImportAlert(z);
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                Toast.makeText(this, "File not found", Toast.LENGTH_SHORT).show();
            } catch (MyException e) {
                e.printStackTrace();
                Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void showImportAlert(final Ziel ziel) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        final TextView tv = new TextView(this);
        String text = getResources().getString(R.string.text_import);
        text = String.format(text, ziel.getName(), ziel.getNord(), ziel.getOst());
        tv.setText(text);
        tv.setTypeface(getResources().getFont(R.font.font_digital));
        tv.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
        tv.setGravity(Gravity.CENTER);
        builder.setView(tv);

        builder.setPositiveButton("Navigate",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        newDirection((float) ziel.getNord(), (float) ziel.getOst());
                        dialog.cancel();
                        finish();
                    }
                });
        builder.setNeutralButton("Save",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        try {
                            Zielspeicher zz = Zielspeicher.instance();
                            zz.addAndSave(getBaseContext(), ziel);
                            Toast.makeText(getApplication(), "Destination saved", Toast.LENGTH_SHORT).show();
                        } catch (MyException e) {
                            Toast.makeText(getApplication(), e.getMessage(), Toast.LENGTH_SHORT).show();
                            e.printStackTrace();
                        }
                        dialog.cancel();
                        finish();
                    }
                });
        builder.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        finish();
                    }
                });

        // Change font type of buttons
        final AlertDialog alertDialog = builder.create();
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                Button btnPositive = alertDialog.getButton(Dialog.BUTTON_POSITIVE);
                btnPositive.setTypeface(getResources().getFont(R.font.font_digital));
                btnPositive.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);

                Button btnNeutral = alertDialog.getButton(Dialog.BUTTON_NEUTRAL);
                btnNeutral.setTypeface(getResources().getFont(R.font.font_digital));
                btnNeutral.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);

                Button btnNegative = alertDialog.getButton(Dialog.BUTTON_NEGATIVE);
                btnNegative.setTypeface(getResources().getFont(R.font.font_digital));
                btnNegative.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
            }
        });

        Objects.requireNonNull(alertDialog.getWindow()).addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
        alertDialog.show();
    }

    public void setFragment(int index) {
        viewPager.setCurrentItem(index);
    }

    @Override
    public void onTabSelected(ActionBar.Tab tab, android.app.FragmentTransaction ft) {
        viewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, android.app.FragmentTransaction ft) {
    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, android.app.FragmentTransaction ft) {
    }

    public void newDirection(double nord, double ost) {
        StatsManager.clearStats(getApplication());
        StatsManager.clearOldLocation(getApplication());
        GraphManager.clearGraphData(this.getBaseContext());

        SharedPreferences pref = this.getSharedPreferences("DIRECTION", 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.putFloat("LATITUDE", (float) nord);
        editor.putFloat("LONGITUDE", (float) ost);
        editor.apply();

        if (fragmentCompass != null) {
            fragmentCompass.reload();
        }
        if (fragmentStats != null) {
            fragmentStats.updateStatsText(this);
            fragmentStats.onTimeChanged(this);
        }

        this.setFragment(0);
    }

    public void updateLocationList() {
        if (fragmentLocations != null) {
            fragmentLocations.updateAdapter();
        }
    }

    public void updateSettingsValue(String key, int val) {
        switch (key) {
            case "update_interval":
                serviceGPS.updateRequestRate();
                break;
            case "stats_accuracy":
            case "height_accuracy":
                GraphManager.update_thresholds(key, val);
                StatsManager.update_thresholds(key, val);
                break;
            case "height_smoothing":
            case "speed_smoothing":
                GraphManager.update_thresholds(key, val);
                break;
            default:
                throw new IllegalArgumentException();
        }
    }

    public void hideKeyboard(View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        if (inputMethodManager != null) {
            inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    /**
     * Animate click of button by making it invisible for a short time
     */
    public void animation(final View view) {
        view.setVisibility(View.INVISIBLE);
        view.postDelayed(new Runnable() {
            public void run() {
                view.setVisibility(View.VISIBLE);
            }
        }, 50);
    }

    public boolean isPaused() {
        SharedPreferences pref = this.getSharedPreferences("STATS", 0);
        boolean paused = pref.getBoolean("PAUSED", false);
        return paused;
    }

    public void savePlayPause(boolean paused) {
        SharedPreferences pref = this.getSharedPreferences("STATS", 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean("PAUSED", paused);
        editor.apply();

        // Update service
        serviceGPS.updateClock();
    }

    @Override
    public void onLocationChanged() {
        if (fragmentCompass != null) {
            fragmentCompass.onLocationChanged(this);
        }
        if (fragmentStats != null) {
            fragmentStats.onLocationChanged(this);
        }
        if (fragmentCreate != null) {
            fragmentCreate.onLocationChanged(this);
        }
    }

    @Override
    public void onTimeChanged() {
        if (fragmentCompass != null) {
            fragmentCompass.onTimeChanged(this);
        }
        if (fragmentStats != null) {
            fragmentStats.onTimeChanged(this);
        }
    }

    @Override
    public void onProviderDisabled() {
        if (fragmentCompass != null) {
            fragmentCompass.onProviderDisabled(this);
        }
    }

    private void createServiceConnection() {
        serviceConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName className, IBinder service) {
                ServiceGPS.LocalBinder binder = (ServiceGPS.LocalBinder) service;
                serviceGPS = binder.getService();
                serviceGPS.setCallbacks(ActivityMain.this);
            }

            @Override
            public void onServiceDisconnected(ComponentName className) {
            }
        };
    }


    private class MyFragmentPagerAdapter extends FragmentPagerAdapter {
        // Following https://stackoverflow.com/a/29288093
        // to fix problems with recreated and not attached fragments in split screen mode

        MyFragmentPagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @Override
        public int getCount() {
            return 6;
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {
            // Do NOT try to save references to the Fragments in getItem(),
            // because getItem() is not always called. If the Fragment
            // was already created then it will be retrieved from the FragmentManger
            // and not here (i.e. getItem() won't be called again).
            switch (position) {
                case 0:
                    return new FragmentCompass();
                case 1:
                    return new FragmentStats();
                case 2:
                    return new FragmentLocations();
                case 3:
                    return new FragmentCreate();
                case 4:
                    return new FragmentSettings();
                case 5:
                    return new FragmentHelp();
                default:
                    throw new IllegalArgumentException();
            }
        }

        @NonNull
        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            // Here we can finally safely save a reference to the created
            // Fragment, no matter where it came from (either getItem() or
            // FragmentManger). Simply save the returned Fragment from
            // super.instantiateItem() into an appropriate reference depending
            // on the ViewPager position.

            Fragment createdFragment = (Fragment) super.instantiateItem(container, position);
            // save the appropriate reference depending on position
            switch (position) {
                case 0:
                    fragmentCompass = (FragmentCompass) createdFragment;
                    break;
                case 1:
                    fragmentStats = (FragmentStats) createdFragment;
                    break;
                case 2:
                    fragmentLocations = (FragmentLocations) createdFragment;
                    break;
                case 3:
                    fragmentCreate = (FragmentCreate) createdFragment;
                    break;
                case 4:
                    fragmentSettings = (FragmentSettings) createdFragment;
                    break;
                case 5:
                    fragmentHelp = (FragmentHelp) createdFragment;
                    break;
                default:
                    throw new IllegalArgumentException();
            }
            return createdFragment;
        }
    }
}